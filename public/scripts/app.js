
var packetsTotal = 0;

var ws;

function init() {
    
    ws = new WebSocket("ws://ece-iot.clarkson.edu:3000/client");
        
        var speedEl = document.getElementById("speedVal");
        var steeringEl = document.getElementById("steeringVal");
        var modeEl = document.getElementById("modeVal");
        var runningEl = document.getElementById("runningVal");
        var signatureEl = document.getElementById("signatureVal");
        var packetsEl = document.getElementById("packetsVal");
    
        var speedSliderEl = document.getElementById("speedSlider");
        var steeringSliderEl = document.getElementById("steeringSliderEl");
    
        ws.addEventListener('message', function (event) {
            var data = JSON.parse(event.data);
            
            packetsTotal++;
            packetsEl.innerHTML = packetsTotal;
            
            switch(data.opcode) {
                case 0:
                    
                    speedEl.innerHTML = speedPercentage(data.param) + "%";
                    break;
                    
                case 1:
                    steeringEl.innerHTML = scale(data.param, 100, 200, -45, 45).toFixed(2) + "°";
                    break;
                case 2:
                    modeEl.innerHTML = data.param;
                    break;
                case 3:
                    runningEl.innerHTML = data.param ? "True" : "False";
                    break;
                case 4:
                    signatureEl.innerHTML = data.param;
                default:
                    break;
            }
        });
        

}
   
function sendVal(op,val) {

    if (ws) ws.send([op, val]);

}

function clearPackets() {
    var speedEl = document.getElementById("speedVal");
    var steeringEl = document.getElementById("steeringVal");
    var modeEl = document.getElementById("modeVal");
    var runningEl = document.getElementById("runningVal");
    var signatureEl = document.getElementById("signatureVal");
    var packetsEl = document.getElementById("packetsVal");
    packetsTotal = 0;
    packetsEl.innerHTML = 0;
    
}

function toggleActiveMode() {
    sendVal(6,0);
}

function setMode(mode) {
    
    sendVal(5, mode);
    
}

function speedPercentage(val) {
    
    if (val < 150) return 0;
    
    else return (val - 150) * 2;
    
}

function scale(num, in_min, in_max, out_min, out_max) {
    
    if (num < 100) {
        return 0
    } else {
        return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
  
}